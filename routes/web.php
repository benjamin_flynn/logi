<?php
use App\Sales_data;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['sales_data' => Sales_data::orderBy('pos_update','desc')->first()]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/compare', function() {
  return View::make('pages.compare');
});
Route::post('/compare/upload', 'ApplyController@upload');

Route::get('/elchef', 'SalesController@process');
Route::get('/reports', 'SalesController@index');