<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Absentee Reports</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .reports {
                width:  20%;
                height: 200px;
                float: left;
                margin-right: 4%;
                border: 1px solid #eee;
                font-weight: normal;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Todays Sales Reports
                </div>

                <div class="reports">
                    <h2>Completed</h2>
                    <h3>$ {{$sales_data->completed_sales_value}}</h3>
                </div>
                <div class="reports">
                    <h2>Open</h2>
                    <h3>$ {{$sales_data->open_sales_value}}</h3>
                </div>
                <div class="reports">
                    <h2>Total</h2>
                    <h3>$ {{$sales_data->total_sales_value}}</h3>
                </div>
                <div class="reports">
                    <h2>% Cash</h2>
                    <h3>{!!round($sales_data->cash_percentage,2)!!}%</h3>
                </div>

                <p>Updated {{Carbon\Carbon::parse($sales_data->pos_update)->format('m-d-y h:i')}}</p>
            </div>
        </div>
    </body>
</html>
