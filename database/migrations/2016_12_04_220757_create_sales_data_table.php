<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_data', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('completed_sales_value',7,2);
            $table->integer('completed_sales_accounts')->unsigned();
            $table->decimal('completed_sales_accounts_ave',7,2);
            $table->integer('completed_sales_people')->unsigned();
            $table->decimal('completed_sales_people_ave',7,2);
            $table->decimal('open_sales_value',7,2);
            $table->integer('open_sales_accounts')->unsigned();
            $table->decimal('open_sales_accounts_ave',7,2);
            $table->integer('open_sales_people')->unsigned();
            $table->decimal('open_sales_people_ave',7,2);
            $table->decimal('total_sales_value',7,2);
            $table->integer('total_sales_accounts')->unsigned();
            $table->decimal('total_sales_accounts_ave',7,2);
            $table->integer('total_sales_people')->unsigned();
            $table->decimal('total_sales_people_ave',7,2);
            $table->decimal('cash_value',7,2);
            $table->integer('cash')->unsigned();
            $table->decimal('card_value',7,2);
            $table->integer('card')->unsigned();
            $table->decimal('tips_value',7,2);
            $table->decimal('total',7,2);
            $table->timestamp('pos_update');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_data');
    }
}
