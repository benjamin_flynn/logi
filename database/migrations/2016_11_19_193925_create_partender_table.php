<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partenders', function (Blueprint $table) {
           $table->increments('id');
           $table->string('insert_id');
           $table->string('product_name');
           $table->integer('container_size_ml');
           $table->integer('total_quantity_on_hand');
           $table->integer('ending_quantity_ml');
           $table->timestamps();
       });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("partenders");
    }
}
