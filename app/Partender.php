<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partender extends Model
{
   public $fillable = ['insert_id','product_name','container_size_ml','total_quantity_on_hand','ending_quantity_ml'];
}