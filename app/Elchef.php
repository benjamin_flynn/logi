<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elchef extends Model
{
   public $fillable = ['insert_id','product_name','ending_quantity_ml'];
}