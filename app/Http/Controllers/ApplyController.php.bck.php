<?php namespace App\Http\Controllers;
use Input;
use Validator;
use Redirect;
use Request;
use Session;
use Excel;
use Carbon\Carbon;
use App\Partender;
use App\Elchef;



class ApplyController extends Controller {

public function upload() {

  // getting all of the post data
  //$file = array('stock' => Input::file('stock'));
  //$file2 = array('sales' => Input::file('sales'));
  //dd($file);
  // setting up rules
  //$rules = array('stock' => 'required'); //'file' => 'required | mimes:application/vnd.ms-excel',  and for max size max:10000
  // doing the validation, passing post data, rules and the messages
  //$validator = Validator::make($file, $rules);
  //$rules2 = array('sales' => 'required'); //'file' => 'required | mimes:application/vnd.ms-excel',  and for max size max:10000
  
  //$validator2 = Validator::make($file2, $rules2);
  //if ($validator->fails() || $validator2->fails()) {
    // send back to the page with the input data and errors
    //return Redirect::to('upload')->withInput()->withErrors('File upload failed');
 // }
  //else {
    // checking file is valid.
    //if (Input::file('stock')->isValid() && Input::file('stock')->isValid()) {
      /*$destinationPath = public_path().'/uploads/stock/'; // upload path
      $extension = Input::file('stock')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      Input::file('stock')->move($destinationPath, $fileName); // uploading file to given path
    
      $destinationPath2 = public_path().'/uploads/sales/'; // upload path
      $extension2 = Input::file('sales')->getClientOriginalExtension(); // getting image extension
      $fileName2 = rand(11111,99999).'.'.$extension2; // renameing image
      Input::file('sales')->move($destinationPath2, $fileName2); // uploading file to given path
      
      // sending back with message
      //Session::flash('success', 'Inventory Upload successfully'); 
      //return Redirect::to('upload');
      Excel::selectSheets('All')->load($destinationPath.$fileName, function($reader) {
        $partender = $reader->select(array('product_name','container_size_ml','total_quantity_on_hand'))->get();
        dd($partender);
      });
       
      //filenaem vars not working
      Excel::selectSheets('Usage')->load($destinationPath.$fileName, function($reader2) {
        $elchef = $reader2->select(array('product_name','ending_quantity_ml'))->get();
      });

      $partender->each(function($row) {
        $elchef->each(function($row){
          if($elchef->get('product_name') == $partender->get('product_name')){
            $pt_qty = $partender->get('container_size_ml') * $partender->get('total_quantity_on_hand');
            $partender->put('partender_qty_ml',$pt_qty);
            $partender->put('el_chef_qty_ml',$elchef->get('ending_quantity_ml'));
          }
        });  
      });
      dd($partender);
        */
     /* $current_time = Carbon::now()->toDayDateTimeString();       
      if(Input::hasFile('stock')){
        $path = Input::file('stock')->getRealPath();
        $partender = Excel::selectSheets('All')->load($path, function($reader) { 
          //
        })->get();
      
      if(!empty($partender) && $partender->count()){
        foreach ($partender as $key => $value) {
          $end_ml = $value->container_size_ml * $value->ending_quantity_ml;
          $insert[] = ['insert_id' => $current_time, 'product_name' => $value->product_name, 'container_size_ml' => $value->container_size_ml, 'total_quantity_on_hand' => $value->total_quantity_on_hand, 'ending_quantity_ml' => $end_ml ];
        }
        if(!empty($insert)){
          DB::table('partender')->insert($insert);
          dd('Insert Record successfully.');
        }
     }*/
     //$current_time = Carbon::now()->toDayDateTimeString();
     $current_time = '123456789';
    try {
      Excel::selectSheets('All')->load(Input::file('stock'), function ($reader) {

        $results = $reader->select(array('product_name','container_size_ml','total_quantity_on_hand'))->get();

        $results->each(function($row){
          $partender = new Partender;
          $partender->insert_id               = '123456789'; 
          $partender->product_name            = $results->product_name;
          $partender->container_size_ml       = $results->container_size_ml;
          $partender->total_quantity_on_hand  = $results->total_quantity_on_hand;
          $partender->ending_quantity_ml      = $results->container_size_ml * $reader->total_quantity_on_hand;
          $partender->save();
        });
        
      });
      Excel::selectSheets('Usage')->load(Input::file('sales'), function ($reader) {

        $results = $reader2->select(array('product_name','ending_quantity_ml'))->get();

        $reader->each(function($row){
          $elchef = new Elchef;
          $elchef->insert_id               = '123456789';
          $elchef->product_name            = $results->product_name;
          $elchef->ending_quantity_ml      = $results->ending_quantity_ml;
          $elchef->save();
        });
      });
      
      $partender = Partender::all();
      //dd($partender);
      
      \Session::flash('success', 'Inventories uploaded successfully.');
        return Redirect::to('compare');
      } catch (\Exception $e) {
        \Session::flash('error', $e->getMessage());
        return Redirect::to('compare');
      }
      
    
    //}
   // else {
      // sending back with error message.
     // Session::flash('error', 'Inventory uploaded file is not valid');
     // return Redirect::to('compare');
   // }
  //}

}
}