<?php namespace App\Http\Controllers;
use Input;
use Validator;
use Redirect;
use Request;
use Session;
use Excel;
use Carbon\Carbon;
use App\Partender;
use App\Elchef;



class ApplyController extends Controller {

public function upload() {

    //$current_time = Carbon::now()->toDayDateTimeString();
    //dd($current_time);
    $current_time = time();
    try {
      Excel::selectSheets('All')->load(Input::file('stock'), function ($reader) use ($current_time) {

        //dd($current_time);

        $results = $reader->select(array('product_name','container_size_ml','total_quantity_on_hand'))->get();

        $results->each(function($row) use ($current_time) {
          $partender = new Partender;
          $partender->insert_id               = $current_time; 
          $partender->product_name            = $row->product_name;
          $partender->container_size_ml       = $row->container_size_ml;
          $partender->total_quantity_on_hand  = $row->total_quantity_on_hand;
          $partender->ending_quantity_ml      = $row->container_size_ml * $row->total_quantity_on_hand;
          $partender->save();
        });
        
      });
      Excel::selectSheets('Usage')->load(Input::file('sales'), function ($reader) use ($current_time) {

        $results = $reader->select(array('product_name','ending_quantity_ml'))->get();

        $results->each(function($row) use ($current_time) {
          $elchef = new Elchef;
          $elchef->insert_id               = $current_time;
          $elchef->product_name            = $row->product_name;
          $elchef->ending_quantity_ml      = $row->ending_quantity_ml;
          $elchef->save();
        });
      });
      
      $partenders = Partender::where('insert_id', $current_time)->get();
      $alerts = array();
      //loop and compare with elchef
      foreach($partenders as $partender) {
        $elchef = Elchef::where('insert_id', '=', $current_time)->where('product_name', '=', $partender->product_name)->where('ending_quantity_ml', '!=', $partender->ending_quantity_ml)->first();
        if(is_object($elchef)){
          $difference = $partender->ending_quantity_ml - $elchef->ending_quantity_ml;
          $alerts[] = array('Product Name' => $partender->product_name, 'Partender' => $partender->ending_quantity_ml, 'ElChef' =>$elchef->ending_quantity_ml, 'Difference' => $difference);
        }
      }
      //dd($alerts);
      Excel::create('Filename', function($excel) use ($alerts) {

        $excel->sheet('Comparison Alerts', function($sheet) use ($alerts) {

          $sheet->fromArray($alerts);
        

        });

      })->export('xls');

      \Session::flash('success', 'Inventories uploaded successfully.');
        return Redirect::to('compare');
      } catch (\Exception $e) {
        \Session::flash('error', $e->getMessage());
        return Redirect::to('compare');
      }
  }




}