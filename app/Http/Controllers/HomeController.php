<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sales_data;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales_data = new Sales_data::first();
        return view('home');
    }
}
