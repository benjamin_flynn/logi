<?php namespace App\Http\Controllers;
use App\Sales_data;
use Carbon;


class SalesController extends Controller {

  public function process() {

    $crawler = \Goutte::request('GET', 'http://quincecompare.dev/uploads/elchef/curltest.html');
    $rows = array();
    //$tr_elements = $crawler->filterXPath('//table/tr');
    // iterate over filter results
    $tds = array();
    foreach ($crawler->filter('td') as $i => $node) {
      // extract the value
      $needles = array('$',',');
      $tds[] = str_replace($needles, '', $node->nodeValue);
     }
    //dump($tds);
    $sales_data = new Sales_data;
    $sales_data->completed_sales_value 		= $tds[2];
    $sales_data->completed_sales_accounts 	= $tds[6];
    $sales_data->completed_sales_accounts_ave = $tds[10];
    $sales_data->completed_sales_people 		= $tds[14];
    $sales_data->completed_sales_people_ave 	= $tds[18];
    $sales_data->open_sales_value 			= $tds[3];
    $sales_data->open_sales_accounts 			= $tds[7];
    $sales_data->open_sales_accounts_ave 		= $tds[11];
    $sales_data->open_sales_people_ave 		= $tds[15];	
    $sales_data->open_sales_people 	 		= $tds[19];
    $sales_data->total_sales_value 			= $tds[4];
    $sales_data->total_sales_accounts 		= $tds[8];
    $sales_data->total_sales_accounts_ave 	= $tds[12];
    $sales_data->total_sales_people 			= $tds[16];
    $sales_data->total_sales_people_ave 		= $tds[20];
    $sales_data->cash_value 					= $tds[22];
    $sales_data->cash 						= $tds[23];
    $sales_data->card_value 					= $tds[25];
    $sales_data->card 						= $tds[26];
    $sales_data->tips_value 					= $tds[28];
    $sales_data->total   						= $tds[31];
    $sales_data->pos_update 					= Carbon::now();
   
    $sales_data->save();
  }


    public function index() {
        $sales_data                     = Sales_data::orderBy('pos_update','desc')->first();
        $sales_data->cash_percentage    = ($sales_data->cash_value/$sales_data->total)*100;  
        //dd($sales_data);
        return view('pages.reports')->with('sales_data', $sales_data);
    }
}