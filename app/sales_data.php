<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sales_data extends Model
{
	 protected $table = 'sales_data';

    public $fillable = ['completed_sales_value','completed_sales_accounts','completed_sales_people','completed_sales_accounts_ave','completed_sales_people_ave','open_sales_value','open_sales_accounts','open__sales_people','open_sales_accounts_ave','open_sales_people_ave','completed_sales_value','total_sales_accounts','total_sales_people','total_sales_accounts_ave','total_sales_people_ave','cash_value','cash','card_value','card','tips_value', 'total', 'pos_update'];
}

